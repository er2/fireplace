package org.er2.fireplace;

import android.app.Activity;
import android.os.Bundle;

/* Typical main activity file
 *
 * (c) Er2 2022 <er2@dismail.de>
 * Zlib License
 */

public class MainActivity extends Activity {
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.main);
  }
}
