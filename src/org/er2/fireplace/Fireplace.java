package org.er2.fireplace;

import java.util.Random;
import java.util.ArrayList;

/* This file is a model and some controller in MVC pattern
 *
 * (c) Er2 <er2@dismail.de>
 * Zlib License
 */

public class Fireplace {
  /* Characters and colors to use */
  /* (yes I like comments in C style) */
  private static String chr = "!\"#$%&*+,./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ\\^_`~ ";
  private static int[] cols = {
    /* 0x OPACITY hex
       Because Android uses opacity before hex
    */
    0xFFfdcf58,
    0xFFf27d0c,
    0xFFf07f13,
    0xFF800909,
  };

  int fsize;               /* Font size */
  public int w, h, col;    /* width, height, columns */
  ArrayList<Integer> fire; /* ArrayList is to allow resize */
  Random rand;             /* Some random :) */

  int ind, cid, hid;       /* column INDex, Color InDex, cHaracter InDex */

  /* Pls no comments */
  public Fireplace() {
    fire = new ArrayList<Integer>();
    rand = new Random();
    ind = -1;
    cid = 0;
    hid = 0;
    fire.add(0);
  }

  public void row() {
    ind = -1;
    next();
  }
  public void next() {
    if(ind + 1 >= fire.size()) return;
    ind++;
    if(fire.get(ind) * fsize > h / 7
    && rand.nextInt(6) == 2)
      fire.set(ind, 0);
    else fire.set(ind, fire.get(ind) + 1);
    cid = rand.nextInt(cols.length);
    hid = rand.nextInt(chr.length());
  }
  public int[] getXY() {
    int[] c = new int[2];
    c[0] = ind * fsize;
    c[1] = h - fire.get(ind) * fsize;
    return c;
  }
  public int getCol() {
    return cols[cid];
  }
  public char getChr() {
    return chr.charAt(hid);
  }

  public void setSize(int w, int h) {
    this.w = w;
    this.h = h;

    fsize = Math.min(w, h) / 32;
    col = w / fsize + 1;
    for(int i = 0; i < col; i++)
      if(i >= fire.size()) fire.add(0);
  }
}

