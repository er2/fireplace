package org.er2.fireplace;

import android.service.dreams.DreamService;

/* Dream service to show fireplace on device
 * instead of just black screen (useful for TVs).
 *
 * (c) Er2 2022 <er2@dismail.de>
 * Zlib License
 */

public class Dream extends DreamService {
  @Override
  public void onAttachedToWindow() {
    super.onAttachedToWindow();

    setInteractive(false);
    setFullscreen(true);
    setContentView(R.layout.dream);
  }
}
