package org.er2.fireplace;

import android.view.View;
import android.graphics.Canvas;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.content.Context;
import android.util.AttributeSet;

import android.util.Log;

/* Fireplace frontend
 * TODO: Optimize this..
 *
 * (c) Er2 2022 <er2@dismail.de>
 * Zlib License
 */

public class FireplaceView extends View {
  Paint paint;   /* Android paint */
  Fireplace fpl; /* Our backend */
  Bitmap bmp;    /* Screenshot of previous frame (thing to be optimized) */

  public FireplaceView(Context ctx, AttributeSet attrs) {
    super(ctx, attrs);
    paint = new Paint();
    fpl = new Fireplace();
    bmp = Bitmap.createBitmap(1, 1, Bitmap.Config.ALPHA_8);
  }

  public void drawFire(Canvas canv) {
    /* draw previous frame and apply transparency
       thing to be optimized
    */
    canv.drawBitmap(bmp, 0, 0, null);
    paint.setColor(0x1E000000);
    canv.drawRect(0, 0, getWidth(), getHeight(), paint);

    /* going through columns */
    for(int i = 0; i < fpl.col; i++) {
      fpl.next();
      paint.setColor(fpl.getCol());
      int[] xy = fpl.getXY();
      canv.drawText("" + fpl.getChr(),
        xy[0], xy[1], paint);
    }
    fpl.row();
    //postInvalidateDelayed(60);
    invalidate();
  }

  @Override
  public void onDraw(Canvas c) {
    /* TODO: To be optimized */
    Bitmap bip = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
    Canvas canv = new Canvas(bip);
    drawFire(canv);
    bmp.recycle();
    bmp = bip;
    c.drawBitmap(bmp, 0, 0, null);
  }
  
  @Override
  protected void onSizeChanged(int w, int h, int ow, int oh) {
    /* Set size directly */
    fpl.setSize(w, h);
  }
}

